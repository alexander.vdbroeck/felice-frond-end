import React, {useEffect} from "react";
import { useDispatch } from "react-redux";
import "./App.css";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

// component imports
/* basic layout */
import Header from "./component/layOut/Header";
import Footer from "./component/layOut/Footer";
import TitleComponent from "./component/layOut/TitleComponent";
import Cookies from "./component/layOut/Cookies";
// pop up stuff
import BasketPopUp from "./component/byFelice/CheckOut/CheckOutDetail/Overview";
/* site felice */
/* site by felice (webshop) */
import Homebyfelice from "./component/byFelice/Homebyfelice";
import ShopCategory from "./component/byFelice/ShopCategory";
import Product from "./component/byFelice/Product";
import Basket from "./component/byFelice/Basket";
import Overview from "./component/byFelice/CheckOut/Overview";

/* site by felice -checkout */

import UserLogin from "./component/byFelice/CheckOut/UserLogin";
import Address from "./component/byFelice/CheckOut/Address";
import { useSelector } from "react-redux";

// action creators
   /* search */
   import { getCategories} from "./data/search";
  import {updatePrices} from "./data/basket"


const App = (props) => {
  
  const dispatch = useDispatch()



  //user store 
  const {loggedIn} = useSelector(state=> state.user)

  const user = useSelector(state=> state.user)
 

  //basket Store
  const {orderDetails,idList,toCheckout, addresComfirm , popUp} = useSelector(state => state.basket)
  const items = idList.length ? idList.length: 0 ;
  //Search store
  const search =   useSelector(state=> state.search);



/* Initialiseren, uitvoeren van action creators , die enkel bij de opstart van de
app nodig zijn  zoals bv de hoofd categorieen pagina. */
const empty = orderDetails.length  == 0;
  useEffect(()=>{
    // als er bij het laden van de applicatie reeds producten in het mandje zitten
    // wordt er op de prijs een update uitgevoerd.
    if(!empty){
      dispatch(updatePrices(idList))

   }
      dispatch(getCategories(false,true,true))
  },[])




  return (
 
    <Router>
          {!user.cookies && (<div className="cookies"><Cookies></Cookies></div>)}
          <div  className={popUp?"basket-pop-up":"basket-pop-up basket-close"}><BasketPopUp/></div>
 

    
          <Header basket={items} />

  
          {/*  Felice website */}
          <Route path="/" render={
            ()=>{return(<><Homebyfelice search={search} />  <TitleComponent title={"by-felice"}/></>)}}
                exact/>
          {/*  Webshop website */}
          <Route path="/byfelice" render={
            ()=>{return(<><Homebyfelice search={search} /> <TitleComponent title={"by-felice"}/></>)}}
                exact/>
          <Route path="/byfelice/producten/:catName/:catId/:parentId" render={(props)=>{
            return (<><ShopCategory search={search} props={props.match}/><TitleComponent title={"webshop-by-Felice"}/></>)
          }} exact />
          <Route path="/byfelice/product/:name/:id" render={(props)=>{
            return(<><Product match={props.match} history={props}/><TitleComponent title={"product"}/></>)
          }}/>
          <Route path="/byfelice/mijn-mandje" render={(props)=> {return(
            <><Basket orderDetails={orderDetails} idList={idList} history={props} exact /><TitleComponent title={"basket"}/></>)
          }} />

          {/*  shop checkout */}
          {/* eerst inloggen / registreren */}
          <Route path="/checkout/user" render={(props)=>{
            return toCheckout ? (<UserLogin history={props.history} user={user}  exact/>)
            :(<Homebyfelice search={search}  exact />)}} />   

          {/* enkel toegankelijk als ingelogd en een bevestiging dat je uit het winkelmandje komt */} 
          <Route path="/checkout/adres" render={(props)=>{
                                    return ! toCheckout ? (<Redirect to="/"/>)
                                          :(<Address props={props}/>)}} />
          {/* bevesting van de bestelling en betalen */}
          <Route path="/checkout/bevestig" render={(props)=>{
                                    return !loggedIn && !addresComfirm ? (<Redirect to="/"/>)
                                          :(<Overview history={props.history}/>)}} />
          <Footer />
        </Router>

  );
};

export default App;
