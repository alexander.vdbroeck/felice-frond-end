import axios from 'axios';

const instance = axios.create({
	baseURL: process.env.REACT_APP_BASE_URI_API
	// baseURL: 'https://127.0.0.1:8000/api/'
});

export default instance;
