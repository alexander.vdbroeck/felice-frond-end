import axios from 'axios';

const instance = axios.create({
	baseURL: process.env.REACT_APP_BASE_URI_API
	// baseURL: 'https://127.0.0.1:8000/api/'
});

instance.interceptors.request.use(
	(config) => {
		const token = localStorage.getItem('token');
		//const token = false;
		if (token) {
			config.headers['Authorization'] = 'Bearer ' + token;
		}
		return config;
	},
	(error) => {
		Promise.reject(error);
	}
);
export default instance;
