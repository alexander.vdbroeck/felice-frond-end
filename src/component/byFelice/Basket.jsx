import React, {useEffect} from "react";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
// components
import PageHeader from './layout/PageHeader'
import PageNavigation from "./CategoryDetail/PageNavigation";
import TableRows from "./BasketDetail/TabelRows";
// font awesome
import "../../fondAwsome/fontawesome";
// stores
import { toCheckout } from "../../data/basket";

export default ({orderDetails, history:{history},idList}) => {
    const dispatch = useDispatch();
    
    // totaal prijs aan de hand van eventuele promoties berekenen 
    const empty = orderDetails.length  === 0;
    let totalPrice = 0
    if(!empty){
         totalPrice = orderDetails.reduce((a,b )=>{
            if(b.promo){
                return ( a + ((b.price - (b.price / 100 * b.promo))* b.quantity))       
            } else{
                return ( a+ (b.price * b.quantity))
            }},0)}
    
        
 
// als men over gaat tot bestellen, krijgt men toegang tot de login pagina
     const handleBasketClick = () =>{
         dispatch(toCheckout())
     }
    //  useEffect(() => {
    //     window.scrollTo(0, 0);
    //   }, );
return(
<>
    <div className="basket">
        {/* subnavigatie om terug te gaan  */}
    <PageNavigation  goBack={history.goBack} />
    <PageHeader pageTitle={"winkelmandje"}/>
    {/* enkel een overzicht houden als er iets in het mandje is */}
    {!empty&&
            (<div className="basket-container">
            <section className="basket-product-list">
                <table>
                    <thead>
                    <tr>
                        <th colSpan="2" className="header-product">producten</th>
                        <th  className="mobile-display-none" >aantal</th>
                        <th className="mobile-display-none">prijs</th>
                    </tr>
                    </thead>
                    <tbody>
                    {orderDetails.map((product)=>
                        (<TableRows key={`${product.id}basket`} product={product}/>))}
                     </tbody>
                </table>
            </section>
            <section className="basket-overview">
                <div className="head">
                    <div>
                        <p>subtotaal</p>
                    </div>
                    <div>
                        {/* Port kosten */}
                        <p>€ {totalPrice.toFixed(2)}</p>
                    </div>
                </div>
                <div className="middel">
                    <p> Levering 1-3 dagen. Gratis bij verzendingen vanaf 30€</p>
                </div>
                < div className="bottem">
                    <div className="total">
                        <div className="deliv">
                            <div>
                                <p>Thuis afleveren</p>
                            </div>
                             <div>
                                <p>{((totalPrice > 30)?"Gratis":"€5")}</p>
                            </div>
                        </div>
                        <div className="grand-total">
                            <div>
                                <p>Totaal</p>
                            </div>
                            <div>
                        <p>€ {(totalPrice > 30? totalPrice.toFixed(2): (totalPrice  +5).toFixed(2))}</p>
                            </div>
                        </div>
                    </div>
                    <div className="basket-submit">
                        <form>
                        <NavLink to="/checkout/user" onClick={handleBasketClick}>   <button> bestellen </button></NavLink>
                        </form>
                    </div>
                </div>
            </section>
        </div>)
        }
        {/*  als er geen producten zijn de gebruiker verwijzen naar de homepage */}
        {empty&&   
             <div className="empty">
                 <p>Er zit nog niets in uw mandje.....</p>
                 <NavLink  to="/byfelice">Snel naar de winkel</NavLink>
            </div>
         }
  
    </div>

</>)

};