import React from "react";
import { useDispatch } from "react-redux";
import {  NavLink } from "react-router-dom";

// components



// font awesome
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../../fondAwsome/fontawesome";

// stores
import { addItem, removeItem } from "../../../data/basket";

export default ({product}) =>{
    const dispatch = useDispatch();
    let promotion = false;

    let totPrice = product.price
    if(product.promo){
        promotion  = product.price - (product.price /100 * product.promo);
        totPrice = promotion;
    }
    const handleAddProductClick = (id)=> e =>{
         dispatch(addItem(id))
    } 
    const handleRemoveProductClick = (id, remove)=> e =>{
        if(remove){
            dispatch(removeItem(id,false))
        }else{
            dispatch(removeItem(id,true))
        }
     }

    return(
        <>
                            <tr key={product.id}>
                            <td  className="image-basket ">
                                <NavLink to={`/byfelice/product/${product.name}/${product.id}`}>
                                  <img alt="afbeelding product" src={process.env.REACT_APP_BASE_URI_PROD_SMALL + product.image}/>
                                </NavLink> 
                            </td>
                            <td className="product-name">
                                 <div >
                                    <p className="name">{product.name}</p>
                                    <p className="mobile-display-none">{product.miniDiscription}</p>
                                    
                                    <div className="price desktop-display-none">
                                        <p className={promotion ? "price price-background" : ""}>€ {product.price.toFixed(2)}</p>    
                                        {promotion  && (
                                <div className="promo-overlay">
                                    <p>{`€ ${promotion.toFixed(2)}`}</p>
                                </div> )}  
                                        <p>tot: €{(totPrice * product.quantity).toFixed(2)}</p>
                                    </div>
                                    <div className="desktop-display-none">
                                    <div className="quantity">
                                           {(product.quantity > 1)&&  <div className="min" onClick={ handleRemoveProductClick(product.id)}> <FontAwesomeIcon icon={['fas','minus']} /></div>}
                                    <div className="quantity-display">
                                        <div>{product.quantity}
                                        </div>
                                        </div>
                                    <div  className="plus" onClick={handleAddProductClick(product.id)}> <FontAwesomeIcon icon={['fas','plus']}  /></div>                           
                                    </div>
                              
                                 </div>

                                    <p className="delete-product" onClick={ handleRemoveProductClick(product.id,true)}>Verwijder</p>
                                </div>
                                </td>
                            <td className="mobile-display-none" >
                                <div className="quantity desktop-display-none">
                                   {(product.quantity >1)&&  <div className="min"  onClick={ handleRemoveProductClick(product.id)}> <FontAwesomeIcon icon={['fas','minus']} /></div>}
                                    <div className="quantity-display"><div>{product.quantity}</div></div>
                                    <div  className="plus" onClick={handleAddProductClick(product.id)}> <FontAwesomeIcon icon={['fas','plus']} /></div>
                                   
                                </div>
                            </td>
                            <td className="mobile-display-none">
                                <div className="price">
                                        {promotion  && (
                                            <div className="promo-overlay">
                                    <p>{`€ ${promotion.toFixed(2)}`}</p>
                                    <p className="promo-discr">{product.promoDescription}</p>
                                </div> )}
                                    <p className={promotion ? "price price-background" : ""}>€ {product.price.toFixed(2)}</p> 
                                    <p>tot: €{(totPrice * product.quantity).toFixed(2)}</p>   
                                </div>
                            </td>
                        </tr>
        </>
    )
}