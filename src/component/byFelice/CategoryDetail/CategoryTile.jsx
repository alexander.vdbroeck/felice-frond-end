import React from "react";
import { Link } from "react-router-dom";

// component imports


// helpers
import { slugify } from "../../../helpers/sanitizers";

export default ({category}) => {
return (<><h3>{category.name}</h3>

{category.image && <img src={process.env.REACT_APP_BASE_URI_CAT + category.image} alt={category.name}></img>}
<Link to={`/byfelice/producten/${slugify(category.name)}/${category.id}`}><p>naar de {category.name}</p>
</Link><hr/></>)
};