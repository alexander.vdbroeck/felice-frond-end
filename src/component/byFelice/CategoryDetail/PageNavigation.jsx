import React from "react";
import {  useSelector} from "react-redux";
import {  NavLink } from "react-router-dom";

export default ({parentCat = 0,goBack = false,home = false})=>{

        const {MainCategories} = useSelector(state=> state.search)    


    return(
        <>
        <div className="categorie-nav">
        <ul>{goBack && (<li className="goback"  onClick={()=>{goBack()}}><p>Go Back</p></li>)}
                {home && <li> <NavLink  to="/byfelice/">Home</NavLink></li>}
                 {MainCategories&& MainCategories.map((cat)=>{return(<li key={cat.id} className={(cat.id === parentCat?"active":"")}>
                 <NavLink  to={`/byfelice/producten/${cat.name}/${cat.id}/${cat.id}`}>
                {cat.name}</NavLink></li>)})}

        </ul>
        </div>

        </>
    )
}