import React from "react";
import { Link } from "react-router-dom";
// helpers
import { slugify } from "../../../helpers/sanitizers";

export default ({category, activePage}) => {

return (<>

    <div className={String(category.id) === String(activePage) ?"cat-tyle-big active-cat":"cat-tyle-big"}>
        <figure><Link title={`naar de categorie: ${category.name}`} to={`/byfelice/producten/${slugify(category.name)}/${category.id}/${category.parentById}`}>
            {category.image && 
                <img src={process.env.REACT_APP_BASE_URI_CAT_SMALL + category.image} alt={category.name}/>}
                <figcaption>
                  <p>   {category.name}</p>
                 
                </figcaption></Link>
                
        </figure>
    </div>
    <div className={String(category.id) === String(activePage)?"cat-tyle-small active-cat-small":"cat-tyle-small"}>
        <span>
       <Link title={`naar de categorie: ${category.name}`} to={`/byfelice/producten/${slugify(category.name)}/${category.id}/${category.parentById}`}>
        {category.name}</Link></span>
           
    </div>
    </>)
};