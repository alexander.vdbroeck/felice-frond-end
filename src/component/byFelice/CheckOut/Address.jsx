import React, { useEffect, useState} from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import {getUserInfo } from "../../../data/user";

// components
import PageHeader from "../layout/PageHeader";
import CheckOUtNav from "../layout/CheckoutNav";
import AddressForm from "./CheckOutDetail/AddressForm";
import NewAddressForm from "./CheckOutDetail/NewAddressForm";
import Overview from "./CheckOutDetail/Overview";

export default ({props}) => {
 
    const dispatch = useDispatch();
    const {deliveryAddress,billingAddress,message} = useSelector((state)=> state.user);
    const[addBillingAddres, setBillingAddress] = useState(false);
    useEffect(()=>{
        dispatch(getUserInfo(localStorage.getItem('id')));
    },[dispatch])
    useEffect(() => {
        window.scrollTo(0, 0);
      }, );

return(<>
     <div className="page-container extra-space">
        <div className="page-overview-container">
            <PageHeader pageTitle={"checkout"}/>
            <CheckOUtNav page={"naar-waar?"}/>
            <section className="checkout-window">
    
            <div className="address-forms">
            {message && <div><p className="address-forms-message">{message}</p></div>}
                {!deliveryAddress &&<NewAddressForm history={props.history} billing={false}/>} 
                {deliveryAddress &&<AddressForm  billing={false}/>} 
                {!billingAddress && <div onClick={()=>{setBillingAddress(!addBillingAddres)}}><p className="button-address-confirm">factuur naar een adres? klik hier.</p></div>}
                {addBillingAddres &!billingAddress &&<NewAddressForm  billing={true}/>}  
                {billingAddress &&<AddressForm  billing={true}/>}
                {deliveryAddress&& <div className="confirm-address"><div> <NavLink to="/checkout/bevestig">   <button> Naar betalen  </button></NavLink></div></div>}
            </div>
            {/* <Overview/>   */}
            </section>
        
     
     

        </div>
    </div>
        </>)
};