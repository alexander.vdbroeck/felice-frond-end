
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
// ophalen user data van de store
import {edditAddres } from "../../../../data/user";
import cities from '../../../../helpers/city'
import { useEffect } from "react";
import PropagateLoader from "react-spinners/PropagateLoader";
import { css } from "@emotion/core";
// css loader
const override = css`
display: block;
width: 15px !important;
`;

export default ({billing})=> {
    let addressType = "deliv";
    let formName = "leverings-adres";
    if(billing){
        addressType = "home";
        formName = "facturatie-adres"
    }
    const{ deliveryAddress, billingAddress,addresComfirm, loading,updateSucces} = useSelector(state=>state.user);
    let address = billing? billingAddress: deliveryAddress;
    const dispatch = useDispatch();

    // stedenlijst ophalen:
    const list = cities();

    // lijst om de city id uit te halen
    let initCity = [];
    let street = address.street? address.street : "";
    let houseNr = address.houseNr? address.houseNr : "";
    let busNr = address.busNr? address.busNr : "";
    let postCode = address.postCode? address.postCode : "";
    let cityId = address.cityId? address.cityId : "";
    let city = address.street? address.city : "";
    let addresId = address.addresId? address.addresId : "";


    // formulier uitschakelen als er reeds een adres is
    const [disabledInput , setDisabled] = useState(true)

    // als er reeds een adres is de lijst om de city id uit te halen instellen om bestaande stad
    initCity = [{ name: city , id:  cityId, zipcod : postCode, checked:true}];
    const [cityOptions, setCityOption] = useState(initCity)

    // react form hooks defeault values
    const {register ,handleSubmit, errors} = useForm({
        defaultValues:{
            street,
            houseNr,
            busNr , 
            postCode,
            cityId,
            city
        },validateCriteriaMode: "all",
        mode: "onBlur"
    });

    // on submit actions check (in de velden)
    const onSubmit = ({street,houseNr, busNr ,cityId})=>{
           
            dispatch(edditAddres(addresId,street,houseNr,busNr,cityId,addressType))

    }
    // opvragen van de stadnaam en stad code bij het ingeven van de postcode
    const handleCityCode =(e)=>{
        console.log(e.target.value.toString().length)

            setCityOption(list.filter((item)=> {return item.zipcode == e.target.value})) 
     
    }

    useEffect(()=>{
        if(addresComfirm){
            setDisabled(true)
        }
    },[deliveryAddress,billingAddress,addresComfirm])
    useEffect(()=>{
        if(updateSucces){
            setDisabled(true)
        }
    },[updateSucces])

    return(
        <>
        <div className={disabledInput?"add-address-form form-disabled":"add-address-form"}>
        <h3>{formName}</h3>
    {deliveryAddress && <div className="form-disabled" onClick={()=>{setDisabled(!disabledInput)}}><p className="button-address-confirm">{!billing ? "Leverings-":"facturatie-"}adres aanpassen</p></div>}
            <form onSubmit={handleSubmit(onSubmit)}>
           
            {errors.street && <div><p className="input-error">{errors.street.message}</p></div>}
            <label htmlFor="street"  >straat</label>
            <div>
                <input disabled={disabledInput} type="text" ref={register({required:"U moet dit veld invullen"})}  name="street" />       
            </div>
            <label disabled={disabledInput} htmlFor="houseNr">huis nummer</label>
            {errors.houseNr && <div><p className="input-error">{errors.houseNr.message}</p></div>}
            <div>
              <input disabled={disabledInput} ref={register({required:"U moet dit veld invullen"})} type="text" name="houseNr"/>
            </div>
            <label   htmlFor="busNr">bus nummer</label>
            {errors.busNr && <div><p className="input-error">{errors.busNr.message}</p></div>}
            <div>
                <input disabled={disabledInput} type="text" name="busNr"  />
            </div>
            <label htmlFor="postCode">postcode</label>
            <div>
                <input disabled={disabledInput}  ref={register({maxLength :4, required:"U moet dit veld invullen"})} onChange={handleCityCode} maxLength={4} type="text" name="postCode"  />
            </div>
            <label htmlFor="cityId">stad </label>
            {errors.cityId && <div><p className="input-error">{errors.cityId.message}</p></div>}
            <div className="city-container">
           {cityOptions.length !== 0 && 
             cityOptions.map((item, i)=>(
                <div key={item.id}>
                   <div> <label className="city-name">{item.name}</label></div>
                    <input className="checkbox-city" disabled={disabledInput} required={true} type="radio" name="cityId" ref={register({required:"U moet dit veld invullen"})} value={item.id}   />
                </div>))}
            </div>
            <input type="text" hidden  name="cityId"  />

            {loading && <div className="loading-form-address"><PropagateLoader
                color={"#36D7B7"}
                css={override}
                loading={loading}
                width={20}  />
            </div>}
            {!disabledInput && <input className={disabledInput ?"display-none":""} type="submit" value="Bevestig uw adres" />}
            
            </form>

        </div>
        </>
    )
}


     