import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { logIn } from "../../../../data/user";
import { useForm } from "react-hook-form";
import GridLoader from "react-spinners/GridLoader";

// css loader
import { css } from "@emotion/core";
const override = css`
display: block;`;
export default ( {history}) =>{
    const dispatch = useDispatch();
    const user =  useSelector(state=> state.user)
    const {message,loading} = user;
    const {register ,handleSubmit, errors} = useForm();
    const onSubmit = ({email,passw})=>{
        // todo verdere error behandeling
        dispatch(logIn(email,passw))
    }


    return(
        <>
        {loading && <div className="loading-form">
        <GridLoader
          color={"#36D7B7"}
          css={override}
          loading={loading}
          size={15}
          margin={2}
    
  
          
        />
      </div>}
        <div className="login-form"> 
        
          <form onSubmit={handleSubmit(onSubmit)}>
              <label htmlFor="email">Email</label>
            
              {message && <div><p className="from-error-field">{message}</p></div>}
            {errors.email && <div><p className="from-error-field">{errors.email.message}</p></div>}
              <div>
              <input className={errors.email ? "input-error":""} type="email" ref={register({required:"U moet een e-mail adres opgeven"})} name="email"/>
              </div>
              <label htmlFor="passw">passwoord</label>
                    {errors.passw && <div><p className="from-error-field">{errors.passw.message}</p></div>}
                <div>
              <input className={errors.passw ? "input-error":""} type="password" ref={register({required:"u moet uw paswoord opgeven",  minLength: {
            value: 3,
            message: "Uw passwoord moet minimum 8 karakters hebben"
          } })} name="passw"/>
                </div>
                {!loading &&
                <div className="submit">
                <div>
          <input type="submit"/>
                </div>
            </div>}
        </form>

        </div>
   
        </>
    )
}
