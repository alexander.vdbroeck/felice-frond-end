
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
// ophalen user data van de store
import {newAddress } from "../../../../data/user";
import cities from '../../../../helpers/city'
import PropagateLoader from "react-spinners/PropagateLoader";
import { css } from "@emotion/core";
// css loader
const override = css`
display: block;
width: 15px !important;
`;

export default ({billing})=> {
    let addressType = "deliv";
    let formName = "leverings-adres";
    if(billing){
        addressType = "home";
        formName = "facturatie-adres"
    }
    const {message, loading} = useSelector(state=> state.user);
    // stedenlijst ophalen:
    const list = cities();
    // lijst om de city id uit te halen
    let initCity = [];
    // als er reeds een adres is de lijst om de city id uit te halen instellen om bestaande stad
    //  hooks
    const [cityOptions, setCityOption] = useState(initCity)
    const dispatch = useDispatch();
    // react form hooks defeault values
    const {register ,handleSubmit, errors} = useForm({
        // by setting validateCriteriaMode to 'all',
        // all validation errors for single field will display at once
        validateCriteriaMode: "all",
        mode: "onBlur"
      });

    // on submit actions check (in de velden)
    const onSubmit = ({street,houseNr, busNr ,cityId})=>{
     
            dispatch(newAddress(street,houseNr,cityId,busNr,addressType))
    }
    // opvragen van de stadnaam en stad code bij het ingeven van de postcode
    const handleCityCode =(e)=>{
            setCityOption(list.filter((item)=> {return item.zipcode == e.target.value}))  
    }

    return(
        <>
        <div className="add-address-form">
            <h3>{formName}</h3>

            <form onSubmit={handleSubmit(onSubmit)}>
            {message && <div><p className="input-error">{message}</p></div>}
            <label htmlFor="street" for="street" >straat</label>
            {errors.street && <div><p className="input-error">{errors.street.message}</p></div>}
            <div>
                <input  type="text" ref={register({required:"Geef uw straat aub", minLength: {
                                                          value: 2,
                                                          message: "uw straat moet minimum 2 karakters hebben"
                                                      },pattern:{value:/[a-zA-Z0-9\s]/,
                                                        message:"enkele letters en cijfers gebruiken aub" }})}  name="street" />       
            </div>
            <label  for="houseNr">huis nummer</label>
            {errors.houseNr && <div><p className="input-error">{errors.houseNr.message}</p></div>}
            <div>
              <input  ref={register({required:"U mag enkel cijfers en letters gebruiken",pattern:{value:/[a-zA-Z0-9\s]/,
                                                    message:"U moet een geldig huisnr ingeven" }})} type="text" name="houseNr"/>
            </div>
            <label   for="busNr">bus nummer</label> 
            <div>
                <input ref={register({pattern:{value:/[a-zA-Z0-9\s]/,
                                                    message:"U moet" }})} type="text" name="busNr"  />
            </div>
            <label for="postCode">postcode</label>
            {errors.cityId && <div><p className="input-error">{errors.cityId.message}</p></div>}
            <div>
                <input   ref={register({maxLength :4, required:true})} onChange={handleCityCode} maxLength={4} type="number" name="postCode"  />
            </div>
            <label for="cityId">stad </label>
            <div className="city-container">
           {cityOptions.length !== 0 && 
             cityOptions.map((item, i)=>(
                <div key={item.id}>
                   <div> <label>{item.name}</label></div>
                    <input required="true" type="radio" name="cityId" ref={register({required:"u moet een stad kiezen"})} value={item.id}   />
                </div>))}
            </div><div className="loading-form-address"><PropagateLoader
                color={"#36D7B7"}
                css={override}
                loading={loading}
                width={20}  />
            </div>
            <input type="text" hidden  name="cityId"  />
            {!loading &&  <input type="submit" value="Bevestig uw adres" />}
            
           
            
            </form>
             
        </div>
        </>
    )
}


     