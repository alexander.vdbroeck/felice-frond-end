import React  from "react";
import { useSelector, useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
//componenten
import ProductDetail from "../../productDetail/OverviewDetaill";
import { closePopupBasket } from "../../../../data/basket";



export default ( {submit}) =>{

    const {orderDetails,popUp} = useSelector(state=> state.basket);
    const dispatch = useDispatch();
    const empty = orderDetails.length  === 0;
    let totalPrice = 0
    if(!empty){
         totalPrice = orderDetails.reduce((a,b )=>{
            if(b.promo){
                return ( a + ((b.price - (b.price / 100 * b.promo))* b.quantity))       
            } else{
                return ( a+ (b.price * b.quantity))
            }},0)}

 
    const handleCloseBasketClick = ()=>{
        dispatch(closePopupBasket());
    }

    return(
        <>
            <section className="checkout-overview" onClick={handleCloseBasketClick}>
                <h2>Overzicht van mijn prodcuten</h2>
                {popUp &&(
                               <div className="bottem-popup">
                               <div onClick={handleCloseBasketClick} className="popup-basket-toBasket">
                                    <NavLink to="/byfelice/mijn-mandje">Naar je mandje</NavLink>
                               </div>
                               <div onClick={handleCloseBasketClick} className="popup-basket-close">
                                    <p>Verder Winkelen</p>
                               </div>
                           </div>
                    )}
                {orderDetails && (orderDetails.map((prod)=><ProductDetail key={prod.id} product={prod}/>))}
                <div className="head">
                    <div>
                        <p>subtotaal</p>
                    </div>
                    <div>
                        <p>€  {totalPrice.toFixed(2)}</p>
                    </div>

                </div>
                <div className="middel">
                    <p> Levering 1-3 dagen. Gratis bij verzendingen vanaf 30€</p>
                </div>
                < div className="bottem">
                    <div className="total">
                        <div className="deliv">
                            <div>
                                <p>Thuis afleveren</p>
                            </div>
                             <div>
                                <p>{(totalPrice > 30?"Gratis":"€5")}</p>
                            </div>
                        </div>
                        <div className="grand-total">
                            <div>
                                <p>Totaal</p>
                            </div>
                            <div>
                        <p>€ {(totalPrice > 30? totalPrice.toFixed(2): (totalPrice  +5).toFixed(2))}</p>
                            </div>
                        </div>
                    </div>
                    {submit&&(
               <div className="basket-submit">
               <form>
               <NavLink to="/checkout/user">   <button> Bevestigen  </button></NavLink>
               </form>
           </div>
                    )}
           
   
                </div>
            </section>
        </>
    )
}