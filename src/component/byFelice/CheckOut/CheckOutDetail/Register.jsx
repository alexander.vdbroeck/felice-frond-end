import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {  registerUser } from "../../../../data/user";
import { useForm } from "react-hook-form";
// loader
import { css } from "@emotion/core";
import PropagateLoader from "react-spinners/PropagateLoader";

export default ( {history}) =>{
  // css loader
const override = css`
display: block;
width: 15px !important;
`;
    const dispatch = useDispatch();
    const  user =  useSelector(state=> state.user)
    const {message, loading} = user
    const {register ,handleSubmit, errors} = useForm({
        // by setting validateCriteriaMode to 'all',
        // all validation errors for single field will display at once
        validateCriteriaMode: "all",
        mode: "onBlur"
      });
      console.log(loading)
    const onSubmit = ({email,passw,voornaam,achternaam, geslacht})=>{
        
       dispatch(registerUser(email,passw,voornaam,achternaam,geslacht));
      
    }

    // todo validatie en erro handling (email / paswoord)
    return(
        <><div className="login-form">
                    
          <form onSubmit={handleSubmit(onSubmit)}>
            <label>Aanhef:</label>
            <select name="geslacht" ref={register({required:"Geef uw aanhef aub"})}>
                <option value="male">De heer</option>
                <option value="female">Mevrouw</option>
            </select>
            <label>Voornaam</label>
            {/* {message && <div><p className="input-error">{message}</p></div>} */}
            {errors.voornaam && <div><p className="input-error">{errors.voornaam.message}</p></div>}
           
            <input type="text" ref={register({required:"Gelieve uw voormaam in te vullen",
                                              minLength: {
                                                        value: 2,
                                                        message: "Uw naam moet minimum 2 karakters hebben",
                                                        
                                                    },pattern:{value:/[a-zA-Z0-9\s]/,
                                                    message:"enkele letters en cijfers gebruiken aub" } })} name="voornaam"/>
            <label>achternaam</label>
            {errors.achternaam && <div><p className="input-error">{errors.achternaam.message}</p></div>}
            <input type="text" ref={register({required:"Gelieve uw achternaam in te vullen"
                                                ,
                                                minLength: {
                                                          value: 2,
                                                          message: "Uw achternaam moet minimum 2 karakters hebben",
                                                          
                                                      },pattern:{value:/[a-zA-Z0-9\s]/,
                                                        message:"enkele letters en cijfers gebruiken aub" }})} name="achternaam"/>
            <label>email</label>
            {errors.email && <div><p className="input-error">{errors.email.message}</p></div>}   
            <input type="email" ref={register({required:"Gelieve uw email in te vullen",pattern:{pattern:/^(([^<>()[].,;:\s@"]+(.[^<>()[].,;:\s@"]+)*)|(".+"))@(([^<>()[].,;:\s@"]+.)+[^<>()[].,;:\s@"]{2,})$/,message:"Dit is geen geldig e-mailadres"}})} name="email"/>
            <label>paswoord</label>
            {errors.passw && <div><p className="input-error">{errors.passw.message}</p></div>}
            {errors?.passw?.types?.pattern && <p className="input-error" >Gebruik een hoodletter, kleine letter en een cijfer</p>}
            <input type="password" ref={register({required:"Gelieve uw paswoord in te vullen"
                                                ,
                                                minLength: {
                                                          value: 8,
                                                          message: "Uw paswoord moet minimum 8 karakters hebben"
                                                      }, pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/})} name="passw"/>
             <div><p>* minimum 8 karakters en gebruik minimum 1 kleine 1 hoofdletter en een cijfer </p></div>
             <div className="register-checkbox">
          
             <label>Ik woon in Belgie:</label>
            
            <input type="checkbox" ref={register({required:"Leveringen kunnen alleen in Belgie"})} name="country"/>
                 </div><div className="form-remark"><p>* Leveringen kunnen alleen in Belgie</p></div> 
                    {errors.country && <div><p className="input-error">{errors.country.message}</p></div>} 
          
            <div className="register-checkbox">
          
          <label>Ik aanvaard de algemene voorwaarden: <a target="_blank" href="https://wdev.be/wdev_alexander/eindwerk/system/img/avw.pdf" rel="noopener noreferrer">naar voorwaarden</a></label>
         
         <input type="checkbox" ref={register({required:"U moet onze algemene voorwaarden aanvaarden"})} name="conditions"/>
              </div>
                 {errors.conditions && <div><p className="input-error">{errors.conditions.message}</p></div>} 
         {!loading && <input type="submit"/> } 
            {message && <div><p className="input-error">{message}</p></div>} 
       {loading &&<div className="loading-form"><PropagateLoader
          color={"#36D7B7"}
          css={override}
          loading={loading}
          width={15}
       
    
  
          
        />
      </div>} 
          
        </form>

        </div>
   
        </>
    )
}