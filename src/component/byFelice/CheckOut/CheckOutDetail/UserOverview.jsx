import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { placeOrder } from "../../../../data/basket";
import {  } from "../../../../data/user";
import GridLoader from "react-spinners/GridLoader";

// css loader
import { css } from "@emotion/core";
const override = css`
display: block;`;



export default ( ) =>{
    const dispatch = useDispatch();
    const {userInfo,deliveryAddress, billingAddress} = useSelector(state=> state.user);
    const {orderDetails, loading, message} = useSelector(state=>state.basket);
    const {firstName,secondName,gender} = userInfo;
    const {postCode,city, street,houseNr, busNr} = deliveryAddress;
    const handleConfirmClick = ()=>{
        dispatch(placeOrder(orderDetails));

    }
    return(
        <> 
        <section className="user-overview">
        {loading &&          <div className="loading-form">
        <GridLoader
          color={"#36D7B7"}
          css={override}
          size={15}
          margin={2}

        />
      </div> }
      {loading  && <p>{message}</p>}
            <h2>We vatten alles nog eens samen:</h2>
            <div><p>{`${gender === "male"?"De heer:": "Mevrouw:"} ${secondName} ${firstName}`}</p></div>
            <div className="delivery"><p>Uw producten worden geleverd op het volgende adres:</p></div>
            <div><p>{`${street} ${houseNr} ${busNr !== "" ? "bus:"+busNr:"" }`}</p></div>
            <div><p>{`te ${postCode} ${city}`}</p></div>
            {billingAddress && (<>
                <div className="billing">
                    <p>het facturatie adres:</p>
                </div>
                <div>
                    <p>{`${billingAddress.street} ${billingAddress.houseNr} ${billingAddress.busNr !== "" ? "bus:"+billingAddress.busNr:"" }`}</p>
                </div>
                <div>
                    <p>{`te ${billingAddress.postCode} ${billingAddress.city}`}</p>
                </div>
            </>)}
            <div className="confirm-button" onClick={()=>{handleConfirmClick()}}>
  
                <button >betaal</button>

                </div>
               
     
        </section>

        </>
    )
}