import React from "react";
import Overview from "./CheckOutDetail/Overview";
import UserOverview from "./CheckOutDetail/UserOverview";
import PageHeader from "../layout/PageHeader";
import CheckOUtNav from "../layout/CheckoutNav";
import {useSelector} from "react-redux";
import { useEffect } from "react";

export default ({history}) => {

    const {status} = useSelector(state => state.basket)
    useEffect(()=>{
        if(status === "pay-ok"){
            setTimeout(()=>{
                history.push('/byfelice')
            },10000)
           
        }
    },[status])
    useEffect(() => {
        window.scrollTo(0, 0);
      }, );
    return(<>
    <div className="page-container">
        <div className="page-overview-container">
            <PageHeader pageTitle={"checkout"}/>
            <CheckOUtNav page={"overzicht"}/>
            <section className="checkout-window">
            {status !==  "pay-ok" &&(<>     
                <Overview/> 
                <UserOverview/> </>    )}
            {status ===  "pay-ok" && (<div><p>Bedankt voor uw aankoop, uw producten komen snel naar u toe.</p>
            <p>u wordt automatisch doorverwezen naar onze homepage</p></div>)}

            </section>
      
      
        </div>
    </div>
    </>)
};