import React, { useEffect, useState } from "react";
import {  useDispatch } from "react-redux";
import {proceedLoggedIn, getUserInfo} from "../../../data/user";



// components
import Overview from "./CheckOutDetail/Overview";
import PageHeader from "../layout/PageHeader"
import CheckOUtNav from "../layout/CheckoutNav"
import Login from "./CheckOutDetail/Login";
import Register from "./CheckOutDetail/Register";


// helpers

export default ({history, user}) => {
    const dispatch = useDispatch();

    const {loggedIn,loggedInCheck,userInfo:{firstName, secondName}} = user;
    const [pleaseRegister, setPleaseRegister] = useState(false);

    const handleProceedClick = ()=>{
        dispatch(proceedLoggedIn(true))
    }


    useEffect(()=>{
        if(user.loggedInCheck){
            history.push('/checkout/adres');
        }
    },[loggedInCheck, history,user.loggedInCheck])
    useEffect(()=>{
   
            dispatch(getUserInfo(localStorage.getItem('id')));  
    },[])
    useEffect(() => {
        window.scrollTo(0, 0);
      }, );

    return(<>
    <div className="page-container">
        <div className="page-overview-container">
                <PageHeader pageTitle={"login-register"}/>
                <CheckOUtNav page={"wie-bent-u?"}/>
                <section className="checkout-window">
                    <Overview submit={false}/>
                    <section className="user-validation">
                        {loggedIn &&(<div className="loggedin-user">
                            <h2>U bent reeds ingelogd</h2>
                            <p>Wilt u door gaan als {firstName + " " + secondName} ? </p>
                        <div className="login-proceed-button" onClick={handleProceedClick}><p>doorgaan als {firstName}</p></div>
                        </div>
                       
                        )}
                        <h2>log-in</h2>
                        <Login history={history}/>
                        <h2>Nog geen account? <span className="please-register" onClick={()=>{setPleaseRegister(true)}}>registreer</span></h2>
                        {pleaseRegister && <Register history={history}/>}
                    </section>

                </section>
            
        
            </div>
         
    </div>

   </>)
};