import React from "react";
import { NavLink } from "react-router-dom";

export default ({search}) => {
  // HoofdCategorien uit de store halen 
  const {MainCategories} = search; 

return (<>

  <div className="page-container">  
    <div className="felice-home-image" >
    </div>
    <section className="intro-webshop">
      <div className="intro-div">
        <article>
          <h2>
          Webshop By Felice
          </h2>
          <p>Hello people,  Welkom in onze webshop by Felice. 
            Verras  de mensen om wie u geeft  met onze originele stijlvolle geschenken, 
            stuur ze een leuke handgemaakte kaart van de hand van Karen Bats , of verwen uzelf met een van onze heerlijke producten.
              Kijk snel hieronder om in onze categorieen te snuisteren...<br></br> <span className="smallscreen-display-none"> 
         "Give a smile, get a smile", dit is het moto waar Felice voor staat.
          Wij creëren met veel liefde de leukste en mooiste geschenken om te geven aan mensen om wie u echt geeft.
           Al onze eigen creaties vindt u  onder ons label "Only By Felice".</span> </p>
        </article>
        <aside>
          <div>
            <div>
            <h2>Benieuwd naar onze eigen creaties?</h2>
            </div> 
            <div className="felice-intro-search">
            <div ><NavLink to="/byfelice/producten/only-by-Felice/38/38" >Only by Felice</NavLink></div>
          </div>
          </div>
          </aside>
        </div>
      </section>
      <section className="felice-categorien">
        <div className="f-c-container">
          <div className="title">
            <h2>Neem een kijkje in onze categorien</h2>
            <p>Zoek in onze belangrijkste categorien, laat u verleiden door al het lekkers en moois.<span className="smallscreen-display-none">
               Wilt u uw aankoop als geschenk versturen? By Felice worden geschenken zo verpakt dat de verpakking een geschenk op zich is.</span></p>
          </div>
         <div className="f-c-cattyle-container">
           {/* hier worden de categorie tegels getoond
           afhankelijk van de schermgrote wordt er ook een ander foto opgehaald */}
          {MainCategories && MainCategories.map((cat)=>{return(   
          //één tegel per categorie 
          <article className="felice-category-article" key={cat.id}>
          <NavLink to={`/byfelice/producten/${cat.name}/${cat.id}/${cat.parentById}`}>
          {/* afhankelijk van de schermbreedte de juiste foto laden */}
          <div className="f-c-img-container">
            <img alt="categorie afbeelding" className="felice-category-img-small" src={ process.env.REACT_APP_BASE_URI_BACKGROUND_SMALL + cat.name + "-home.jpg"}/>
            <img alt="categorie afbeelding" className="felice-category-img-big" src={ process.env.REACT_APP_BASE_URI_BACKGROUND_SMALL + cat.name + "-home.jpg"}/>
            {/* overlay met title */}
              <div className="felice-category-overlay">
                <div className="f-c-title">
                  <div className="upper-line-f"></div>
                  <h2>{cat.name}</h2>
                  <div className="lower-line-f"></div>
                    <div className="f-c-discription bigscreen-display-none">
                    <p dangerouslySetInnerHTML={{__html: cat.description}}></p>
                  </div> 
                </div>
              </div>
            </div>
            {/* onderschrift met omschijving */}
          <div className="f-c-discription smallscreen-display-none">
          <p dangerouslySetInnerHTML={{__html: cat.description}}></p>
          </div> 
          </NavLink>
      </article>)})}
    </div>
    </div>
    </section>
  </div>

  </>)
};