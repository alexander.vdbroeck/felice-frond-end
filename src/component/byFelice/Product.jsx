import React, {useEffect} from "react";
import { searchDetail } from "../../data/search";
import { useDispatch, useSelector } from "react-redux";
import { addItem } from "../../data/basket";
// carousel
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { Carousel } from 'react-responsive-carousel';

// components
import PageNavigation from "./CategoryDetail/PageNavigation";
import { NavLink } from "react-router-dom";

// loader
import { css } from "@emotion/core";
import BarLoader from "react-spinners/BarLoader";
const override = css`
display: block;
margin: 0 auto;
border-color: red;
width: 80vw;
height: 20px;
`;
export default ({match, history:{history}}) => {
  const dispatch = useDispatch();
  // parameters uit de url halen, en het product ophalen
  const prodId = match.params.id
  const {productDetail ,loading,promo} = useSelector(state => state.search);
  const {idList} =  useSelector(state=> state.basket)

  // bij het laden van de pagina en als de parameters wijzigen
    useEffect(()=>{
        dispatch(searchDetail(prodId))
    },[match.params.id])

  // producten toevoegen, en controleren of het een nieuw product is of het aantal verhoogd moet worden
  const handleAddProductClick = (id)=> e =>{
      if(!idList.includes(id)){
      dispatch(addItem(id,true))
      }else{
        dispatch(addItem(id,false))
    }
  } 

  // Naar boven scrollen  bij het laden van de pagina
  useEffect(() => {
    window.scrollTo(0, 0);
  }, );


  return (
    <><div  className="page-container">
      {/* pagina navigatie  */}
         <PageNavigation  goBack={history.goBack} />
      <div className="product-detail-page">
        {/* foto carousel */}
        <div className="sweet-loading">
        <BarLoader
          css={override}
          color={"#36D7B7"}
        
          loading={loading}
        />
      </div>
        {!loading && (<>
        <section className="product-detail-images">
        <Carousel className="carousel">
          {productDetail  && productDetail.images.map((images)=>{
            return(
              <div key={productDetail.id} className="product-slider-img">
                <img src={process.env.REACT_APP_BASE_URI_PROD + images.image} alt={images.imgAlt}/>
              </div>) }) }
        </Carousel>
        {/*  product info en toevoegen aan winkelmandje */}
        </section>
        <aside className="product-detail-summary">
          <span> <h2> {productDetail && productDetail.title}</h2></span>
        <span>{productDetail && 
                (<p className="discr" dangerouslySetInnerHTML={{__html: productDetail.description}}></p>)}
        </span>
        <div className="details">
          {/*  Hier worden de detail gegvens getoond als ze voorhandig zijn*/}
          {productDetail.size && (
            <div> 
              <div>afmetingen:</div>
          <div>{productDetail.size}</div>
            </div>)}
          {productDetail.weight && (
            <div> 
              <div>gewicht:</div>
          <div>{`${productDetail.weight} ${productDetail.weightUnit}`}</div>
            </div>)}
          {productDetail.color && (
            <div> 
              <div>kleur:</div>
          <div>{productDetail.color}</div>
            </div>)} 
            
            <div className="price">
                 {promo   && (
                    <div className="promo-overlay">
                        <p>{`€ ${ (productDetail.productPrice['price'] - (productDetail.productPrice['price'] /100 * productDetail.articlePromotionArray.amount)).toFixed(2)}`}</p>
                 <p className="promo-discr">{productDetail.articlePromotionArray.discr}</p>
                        </div> )}
                   {productDetail && ( <p className={promo?"price-background":"price-nopromo"}>{`€ ${productDetail.productPrice['price'].toFixed(2)}`}</p>)}</div>
        </div>
        <div className="product-detail-inbasket">
        {/* als in reeds in het minkelmandje is  */}
        {!idList.includes(productDetail.id) && (   
        <div onClick={handleAddProductClick(productDetail.id)}> 
                <p>Yes doe hem in mijn mandje</p>
          </div>   )}    
          {idList.includes(productDetail.id) && (   
        <div className="in-basket" > 
                <NavLink to="/byfelice/mijn-mandje"> <p>Yes hij zit in mijn mandje</p></NavLink>
          </div>   )}            
        </div>
        </aside>
      </>)}
      </div>  
    </div>
    </>)
};