import React, {useEffect,useState} from "react";
import { useDispatch } from "react-redux";
import TitleComponent from "../layOut/TitleComponent";
import { css } from "@emotion/core";
import BarLoader from "react-spinners/BarLoader";
// data imports
import { searchProducts,getCategories,getCatDetails, setMessage, resetMessge } from "../../data/search";
import  {getBackground} from "../../helpers/images"
// component imports
import SubCategoryTyle from "./CategoryDetail/subCategoryTyle";
import ProductTyle from "./productDetail/ProductTyle";
import PageHeader from "./layout/PageHeader";
import CategoryDiscription from "./CategoryDetail/CategoryDiscription";
import PageNavigation from "./CategoryDetail/PageNavigation";


//font awesom
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import '../../fondAwsome/fontawesome'


// loader styling
const override = css`
display: block;
margin: 0 auto;
border-color: red;
width: 60vw !important;
height: 20px;
`;
export default ({props:{params},search}) => {
    // categorie uit de headers in hooks steken
    const [formValue, setFormValue] = useState("");
    const dispatch = useDispatch();
    const getImages = getBackground()
    let catToSearch = [];


    // hook voor het openen en sluiten van de mandjes popup

    // de foto voor de categorien laden (deze wordt later in de database gezet)
  
    const {products, categories, loading ,message,catDescripton} = search;
    useEffect(() => {
        window.scrollTo(0, 0);
      }, [loading]);
   
    const [searchBarOpen, setSearchBarOpen] =  useState(false);
 
    /* Ieder keer als de categorie in de url aangepast word, de juiste data ophalen
    dit gaat over de details voor de categorieeën, de info over de huidige categorie
    en de product gegevens, dit gebeurd tijdens het initieel laden van de pagina 
    en bij het wijzigen van de parameters */
    const cat = params.catName;
   
    let catId = params.catId;
    let catName = params.catName;
    let catParent = params.parentId;


    useEffect(()=>{
        catId = params.catId
        catName = params.catName
        catParent = params.parentId
        dispatch(getCatDetails(catId))
        dispatch(searchProducts(catId))
        dispatch(getCategories(catParent))
    },[params.catId,params.parentId])




    // de berichten na 20 seconden verwijderen 
 

    /* de achtergrond images staan nog niet in de database
    , hiervoor worden ze aan de hand van de categorie naam  opgehaald
    Als er afbeeldingen aangepast dienen te worden gebeurd dit
     in de  switch  in helpers/images.js  aangepast*/
    
    const img = getImages(catName)

  

    //formulier handelingen
    const handleSubmitClick =  (e) => {
        e.preventDefault(); 

        if(formValue === ""){
            dispatch(setMessage("u moet iets invullen"));
        }else{
            const searchString= "title="+ formValue
            dispatch(searchProducts(catId,searchString,formValue));
            setFormValue("")
            
    // de berichten na 20 seconden verwijderen 
            setTimeout(()=>{
                dispatch(resetMessge());
                 setSearchBarOpen(false)},15000)
        }
    }

    // to do een scroo to the top button 
    const handleScroll = (event)=>{
      
        const {scrollTop, clientHeight, scrollHeight} = event.currentTarget
        if(scrollHeight - clientHeight < scrollTop +1){
        }      
    }
  
return (
    <>
    <TitleComponent title={params.catName}/>
          <div className="searchIcon" onClick={()=>{setSearchBarOpen(!searchBarOpen)}}><p>zoeken</p></div>
          <div className="searchfield"></div>
          <div className={searchBarOpen?"category-search":"category-search searchbar-closed"}>
            <div>
                <form onSubmit={handleSubmitClick}>
                <div className="search-message">{message&&<p>{message} </p> }</div>
                   <input  placeholder={`zoeken in ${params.catName}`} type="text" onChange={(e)=>{setFormValue(e.target.value)}} value={formValue} />
                    <div className="search" onClick={handleSubmitClick}><FontAwesomeIcon icon={['fas','search']} size="lg" /></div>                  
             
                </form>
            
            </div>
        </div>
    <div   className="category"  >
        {/* een overlay voor foto te bluren */}
        <div className="page-wrapper"> </div>
        {/* background voor categorien  */}
        <div className="category-description-image" > <img src={img} alt="categrie afbeelding"></img></div>

        {/* categorie naam en omschrijving */}

        <PageHeader pageTitle={catName} />
        <CategoryDiscription category={catDescripton.description}/>

        {/* hoofd Categorie navigatie */}
        <PageNavigation parentCat={catDescripton.parentById} home={true}/>
        {!loading   && ( 
        <section className={!loading & categories.length >1  ? "cat-scroller" : "display-none"}> 
            {categories && categories.map((cat)=>
            {return (<SubCategoryTyle key={cat.id} category={cat} activePage={params.catId}/>)})}  
               
        </section>)}
           {/* producten overzicht  */}    
           
        <section className="product-overview" onScroll={handleScroll} >
       
            {! products ?  <div className="sweet-loading">
     
      </div>
                : products.length < 1 ?  (<div className='no-prod' ></div>)
                : ! loading? products.map((prod)=>
                            {return(<ProductTyle key={prod.id} product={prod}/>)} ):   <BarLoader
                            css={override}
                            color={"#36D7B7"}
                          />}
 
        </section>
       
    </div>
    </>)
};
