import React, {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useField } from "../../hooks/formhook";
import ProductTyle from "./productDetail/ProductTyle";
import { searchProducts,getCategories } from "../../data/search";

// components 
import PageHeader from "./layout/PageHeader";

export default () => {
    const {...field} = useField();
    const dispatch = useDispatch();
    const {products, categories, loading,message} = useSelector(state => state.search)

    

    useEffect(()=>{
        dispatch(searchProducts(false,field.value))
    },[]
    )
    return(<>
    <div className="page-container">
    {/* <div className="felice-search-image" >
     </div> */}
     <PageHeader pageTitle={"Zoekt u iets?"} />
     <section className="felice-search-searchbar">

     </section>
     <section className="product-overview">
            {loading && <p>even laden......</p>}
            {products && products.map((prod)=>
            {return(<ProductTyle product={prod}/>)} )}
        </section>


    </div>
    
</>)
}