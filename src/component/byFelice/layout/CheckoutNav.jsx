import React from "react";

export default ({page} ) =>{

    return(
        <>
        <div className="basket-navigation-container">
        <div className="basket-navigation">
            <ul>
                <li className={page === "wie-bent-u?"? "active":""}>
                    <p>login-register</p>
                </li>
                <li className={page === "naar-waar?"? "active":""}>
                    <p>naar-waar?</p>
                </li>    
                <li className={page === "overzicht"? "active":""}>
                    <p>overzicht</p>
                </li>
            </ul>
        </div>

        </div>

        </>
    )
}