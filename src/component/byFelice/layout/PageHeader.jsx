import React from "react";

export default ({pageTitle}) => {
return(
    <>
    <section className="page-title">
        <div className="upper-line"></div>
            <h2>{pageTitle}</h2>
        <div className="lower-line"></div>
    </section>

    </>
    )
}