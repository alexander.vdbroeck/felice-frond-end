import React,{useState, useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import { Link, NavLink} from "react-router-dom";
// helpers
import { slugify } from "../../../helpers/sanitizers";

// stores
import { addItem } from "../../../data/basket";

export default ({product}) => {

const dispatch = useDispatch();
   const {idList} =  useSelector(state=> state.basket)
   const [inBasket, setInbasket] = useState(false)

   const handleAddProductClick = (id)=> e =>{
       setInbasket(true)
       if(!idList.includes(id)){
        dispatch(addItem(id,true))
       }
       
   } 

//    useEffect(() => {
//     window.scrollTo(0, 0);
//   }, );

let promotion = 0
if(product.articlePromotionArray.amount){
    promotion  = product.productPrice['price'] - (product.productPrice['price'] /100 * product.articlePromotionArray.amount);
}
return (<>
        <div className="product-tyle"><Link to={`/byfelice/product/${slugify(product.title) }/${product.id}`}>
             <div className="product-tyle-top">     
                {product.images.length > 0 && (
                <img src={process.env.REACT_APP_BASE_URI_PROD  + product.images[0].image} alt={product.images[0].alt}>
             </img>)} 
             </div>  
             <div className="prodcut-tyle-midle">
                 <span> <p className="p-t-name">{product.title}</p></span>
                 <span className="discription"> <p>{product.miniDescription}</p></span>
                
             </div></Link>
             <div className="product-tyle-bottem">
             <div className="price">
                 {product.articlePromotionArray.amount  && (
                    <div className="promo-overlay">
                        <p>{`€ ${promotion.toFixed(2)}`}</p>
                 <p className="promo-discr">{product.articlePromotionArray.discr}</p>
                        </div> )}
                    <p className={product.articlePromotionArray.amount?"price-background":""}>{`€ ${product.productPrice['price'].toFixed(2)}`}</p></div>
                 <div  className="add-to-card" >
               
                 <div className={!inBasket && !idList.includes(product.id)?"display-basket":"non-display-basket"} onClick={handleAddProductClick(product.id)}>
                 <p>Ja ik wil...</p>
                 </div>
                 {(idList.includes(product.id) || inBasket)&& (
                     <div className="in-basket">
                        <NavLink to="/byfelice/mijn-mandje"> <p>Al in mandje</p></NavLink>
                     </div>
                 )}
                 </div>
  
             </div>

           
  
             
        </div></>)
};