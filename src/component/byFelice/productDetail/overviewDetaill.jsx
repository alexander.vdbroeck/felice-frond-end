import React from "react";




export default ({product} ) =>{
    const {name,image,price,quantity,id} = product;
    let promotion = false;

    if(product.promo){
        promotion  = product.price - (product.price /100 * product.promo);

    }

    return(
        <>
        <div  className="product-overview">
            <div className="row">
                <div className="img">
                <img alt="afbeelding van artikel" src={process.env.REACT_APP_BASE_URI_PROD_SMALL + image}/>
                </div>
                <div className="name">
                    <p>{name}</p>
                </div>
                <div className="price"><p className={promotion? "price-line-true":""}>€ {price.toFixed(2)}</p>
                {promotion  && (
                                <div className="promo-overlay">
                                    <p>{`€ ${promotion.toFixed(2)}`}</p>
                                   
                                </div> )}
                <p>aantal: {quantity}</p>
                {promotion  && <p>tot :€ {(promotion * quantity).toFixed(2)}</p>}
                {!promotion  && <p>tot :€ {(price * quantity).toFixed(2)}</p>}
                </div>
            </div>
        </div>
        </>
    )
}