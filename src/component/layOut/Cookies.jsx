import React from "react";
import { cookieConfirm } from "../../data/user";
import { useDispatch } from "react-redux";

export default () => {
    const dispatch = useDispatch();
    // de cookie confirmation sluiten bij bevestiging
    const  comfirmCookie = () => e =>{
        dispatch(cookieConfirm())
    }
    return (<>
       <div className="cookies-content">
            <h1>Welcome By Felice</h1>
            <p>Dear user, this  webshop has been developed as part of a training to become a full stack developer. (Syntra Mechelen). 
                The products on this site are therefore not real and cannot actually be ordered. In this application you can search for the different products,
                 a shopping basket is kept and you can create an account to complete an order.
                 You will notice that only the links related to these actions are active, other inactive links are just there to give the website some more body.
                  For further questions you can always<a href="mailto:alexander@felice.be" title="mail me" > email me ...</a></p>
            <p>there is also data that is kept while surfing. This is about your shopping cart, and if you create an account, your login details and the address you entered.
                 This information is kept in a database. In addition, data is also stored in cookies, which never contain any passwords or sensitive information.
                  If you would like to see your data deleted after using my application, or an overview of which data we keep about you, it is best to send me an
                   <a href="mailto:alexander@felice.be" title="mail me" > email  ...</a>  <button onClick={comfirmCookie()}>Accept cookies
          
          </button></p>
    
       <img alt="logo" src=" https://wdev.be/wdev_alexander/eindwerk/system/image.php?test.jpg&width=250&image=/wdev_alexander/eindwerk/system/img/logo-2.jpg"></img>
       </div>
       </>)

};