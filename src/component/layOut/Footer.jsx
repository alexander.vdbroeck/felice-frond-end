import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import '../../fondAwsome/fontawesome'


export default () => {
    return (<>
        <footer className='main-footer'>
            <div className="footer-social">
                <ul>
                    <li> 
                        <a href="https://www.instagram.com/batskaren/?hl=nl" title="face-book" >
                        <FontAwesomeIcon icon={['fab', 'facebook']} size="2x" style={{ color: 'white' }} /></a>
                    </li>
                    <li> 
                        <a href="https://www.instagram.com/batskaren/?hl=nl#" title="face-book" >
                        <FontAwesomeIcon icon={['fab', 'instagram']} size="2x" style={{ color: 'white' }} /></a>
                    </li>
                    <li> 
                        <a href="https://www.instagram.com/batskaren/?hl=nl" title="face-book" >
                        <FontAwesomeIcon icon={['fab', 'twitter']} size="2x" style={{ color: 'white' }} /></a>
                    </li>
                    <li> 
                        <a href="https://www.instagram.com/batskaren/?hl=nl#" title="face-book" >
                        <FontAwesomeIcon icon={['fab', 'youtube']} size="2x" style={{ color: 'white' }} /></a>
                    </li>
  
                </ul>
            </div>
            <div className="footer-disclamer">
                <ul>
                    <li>
                    <a  target="_blank" href="https://wdev.be/wdev_alexander/eindwerk/system/img/avw.pdf" rel="noopener noreferrer" title="Algemene voorwaarden">Algemene-voorwaarden</a>

                    </li>
                    <li>
                        <a target="_blank" href="https://wdev.be/wdev_alexander/eindwerk/system/img/avw.pdf" rel="noopener noreferrer" title="Privacy beleid">Privacy-beleid</a>
                    </li>
                    <li>
                    <a target="_blank" href="https://wdev.be/wdev_alexander/eindwerk/system/img/avw.pdf" rel="noopener noreferrer" title="disclamer">disclamer</a>

                    </li>
                </ul>
            </div>
        </footer></>)

};