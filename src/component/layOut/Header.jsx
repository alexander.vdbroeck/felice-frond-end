import React from "react";
import Nav from "./Nav";
import { Link, NavLink } from "react-router-dom";
import Typical from "react-typical";
import logo from '../../img/logo2.png'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import '../../fondAwsome/fontawesome'

export default ({basket}) =>{

    return (
      
    <><header>
    {/*  bovenste contact  balk enkel  bij mobiele apparaten */}
    <div className="mobile-header-contact">
        {/* link naar telefoon nr */}
        <div className="mobile-header-phone">      
            <a href="tel:0032486040406" title="bel me" >
                <FontAwesomeIcon icon={['fas', 'mobile-alt']} size="lg" style={{ color: 'white' }} />
                        {"  "}0486/04.04.06</a>
        </div>
        {/* link naar het email adres */}
        <div className='moblie-header-mail'>
            <p>We {' '}<a href="mailto:alexander@felice.be" title="mail me" >
            <FontAwesomeIcon icon={['far', 'heart']} size="1x" style={{ color: 'white' }} /></a>
             {' '}Mail </p></div>
        </div>

    {/* hoofd header component  */}
    <div className="main-header-contrainer">
        {/* navivatie  */}
            <Nav/>
        {/* logo met typwriter effect */}
        <div className="logo-img-wrapper"><Link title="naar homepage" to='/byfelice/'>
            <img alt="logo" className="logo" src={logo}/></Link><div className="header-banner">
            <Typical steps={['Give a smile',15000,'Get a smile', 15000]} wrapper='span' loop={Infinity}/></div>
        </div>
        {/* mobiel logo naar het winkelmandje  */}
        <div className="mobile-header-icons">
            
        {basket > 0 &&(
                       <div className="header-basket-items">
                       {basket}
                   </div>
            )}
        <Link  title="Zoeken" to="/byfelice/producten/kaarten/1/1">
         
                <FontAwesomeIcon icon={['fas','search']} />
             
            </Link>
            <Link title="naar winkelmandje" to="/byfelice/mijn-mandje">
               
                <FontAwesomeIcon icon={['fas','shopping-bag']} />
                
            </Link>

        </div>
        <div className="desktop-logo">
        <NavLink to="/byfelice">   <h1 >Felice    <span><Typical steps={['Give a smile',15000,'Get a smile', 15000]} wrapper='span' loop={Infinity}/>
             </span>
        </h1></NavLink>
        </div> 
        <div className="desktop-icons">
            {basket > 0 &&(
                       <div className="header-basket-items">
                       {basket}
                   </div>
            )}
     
        <Link title="Zoeken"  to="/byfelice/producten/kaarten/1/1">
               
                <FontAwesomeIcon icon={['fas','search']} />
          
            </Link>
            <Link  title="naar winkelmandje" to="/byfelice/mijn-mandje">
               
                <FontAwesomeIcon icon={['fas','shopping-bag']}  />
               
            </Link>
                </div>
        </div> 

    </header></>)
};