import React, {useState} from "react";


// font awesome 
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import '../../fondAwsome/fontawesome'

// components
import MobileNav from "./details/MobileNav";
import DesktopMainNav from "./details/desktopNav/DesktopMainNav";



export default () => {

    // menu verbergen bij het laden
    const [mobileMenuHide, setMobileHideMenu] = useState(false)

 
    return (<>
    {/* tot lijn 40 is het mobiele menu, eronder de desktop versie */}
            <nav className='main-nav'>
                <div className="mobile-hamburger" onClick={()=>{setMobileHideMenu(!mobileMenuHide)}}>
                    {/* Hamburger menu of kruisje */}
                        <FontAwesomeIcon icon={['fas', 'bars']}size='2x'/>  
                 </div>
                 {/* mobiele menu */}
                 <div className={(mobileMenuHide?"mobile-menu-open":"mobile-menu-closed")} onClick={()=>{setMobileHideMenu(!mobileMenuHide)}}>
                  <div className="close"><FontAwesomeIcon  icon={['fas', 'arrow-left']}size='2x'/></div>  
                    <MobileNav/>
                </div>
                {/* desktop menu */}
                <div className="desktop-navigation-container">
                  <DesktopMainNav/>
                </div>
               
            </nav>
      </>)
};