import React from "react";
import {  NavLink } from "react-router-dom";
import {  useSelector} from "react-redux";
// font awesome 
import '../../../fondAwsome/fontawesome'

export default () => {
    const {MainCategories} = useSelector(state=> state.search)
    return(
        <div className="mobile-menu-list" >
        <ul>      
           <li>
                <NavLink  exact={true} activeStyle={{fontWeight:'bolder' }} to="/byfelice/producten/only-by-Felice/38/38" title="homepage">Only by Felice</NavLink>
            </li>
        <li>
              <NavLink  title="naar de webshop"  exact={true}  activeStyle={{fontWeight:'bolder' }} to="/byfelice">Webshop by Felice</NavLink> 
            </li>
      
           
                     {/* Hoofd categorien ophalen  */}
            <p></p>
            {MainCategories&& MainCategories.map((cat)=>{return(<li key={cat.id} className="sub">
                <NavLink title={cat.name} activeStyle={{fontWeight:'bolder',fontSize:'1.5rem' }}  to={`/byfelice/producten/${cat.name}/${cat.id}/${cat.parentById}`}>
                            {`${cat.name} by Felice`}</NavLink></li>)})}
     
    
            </ul>
   </div>

    )
}
