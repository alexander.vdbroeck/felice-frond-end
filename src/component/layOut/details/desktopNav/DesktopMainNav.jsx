import React, {useState} from "react";
import {  useSelector} from "react-redux";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default () =>{

    const [menuOpen ,setMenuOpen] =  useState(true);
    const[subMenuOpen, setSubMenuOpen] = useState(true)
    const {MainCategories} = useSelector(state=> state.search)
    return(<>
      {/*  hoofd navigatie */}
        <div className="desktop-navigation-main" >
         
            <ul>      
                <li> 
                    <Link title="webshop" to='/byfelice/' onClick={()=>{setMenuOpen(!menuOpen)}}>Shop By felice</Link>
                </li>
                <li>
                    < Link title="Only by Felice" to="/byfelice/producten/only-by-Felice/38/38" >Only by Felice</Link>
                </li>
                <li>
                    <Link to='/byfelice' title="About us" href="#">About Us</Link>
                </li>
            </ul>
       </div>
       {/* drop down menu */}
       <div className={(menuOpen?"desktop-navigation-submenu submenu-closed":"desktop-navigation-submenu")}>
       <div className="desktop-menu-close" onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}> 
       {!menuOpen&&<FontAwesomeIcon  icon={['fas', 'arrow-left']} size='1x'/>}</div>
        <ul>
                <li><Link to='/byfelice' onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}} activestyle={{ fontWeight:'bolder' }}>Webshop by Felice</Link>
                </li>
                <li>
                  <p  title="onze producten" onClick={()=>{setSubMenuOpen(!subMenuOpen)}} >Onze producten</p>
                </li>
                <li>
                   <Link title="Wie zijn wij?"  to='/byfelice' onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}>Waar vindt u Felice?</Link>
                </li>
                <li>
                   <Link to='/byfelice' title="Cadeau ideetjes"onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}>Snoeptafel op uw feest?</Link>
                </li>
                <li>
                  <Link to='/byfelice' title="blog"onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}>Cotton candy by Felice</Link>
                </li>
            </ul>
         
       </div>
       <div className={(subMenuOpen?"desktop-navigation-product submenu-closed-product ":"desktop-navigation-product")}onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}>

       
            <ul>
            {MainCategories&& MainCategories.map((cat)=>{return(<li key={cat.id}>
                            <Link title={cat.name} to={`/byfelice/producten/${cat.name}/${cat.id}/${cat.parentById}`}>
                            {cat.name}</Link></li>)})}
            </ul>
        </div>
       {/* submenu van de drop down */}
       {/* <div className={(subMenuOpen?"submenu-closed-product ":"desktop-navigation-product")}onClick={()=>{setMenuOpen(true);setSubMenuOpen(true)}}>

           <div><p> Onze Producten:</p></div>
            <ul>
            {MainCategories&& MainCategories.map((cat)=>{return(<li> <a title={cat.name}>
                            <Link to={`/byfelice/producten/${cat.name}/${cat.id}`}>
                           {cat.name}</Link></a></li>)})}
            </ul>
       </div> */}
       
       
       </>
        
    )
}