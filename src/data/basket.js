import axios from '../apiSettings/axios';
import axiosToken from '../apiSettings/axiosToken';

/**--------------------- initial state------------------ */

const init = {
	user: false,
	error: false,
	errorMessage: false,
	message: false,
	loading: false,
	toCheckout: false,
	status: 'empty-basket',
	idList: [],
	orderDetails: [],
	popUp: false
};
/**--------------------- Types-------------------------- */
// const START_PRODUCT_SEARCH = "START_PRODUCT_SEARCH";
const ADD_ITEM_INIT = 'ADD_ITEM_INIT';
const ADD_NEW_ITEM = 'ADD_NEW_ITEM';
const ADD_QUANTITY_ITEM = 'ADD_QUANTITY_ITEM';
const REMOVE_ITEM = 'REMOVE_ITEM';
const MINUS_QUANTITY_ITEM = 'MINUS_QUANTITY_ITEM';
const SET_TO_CHECKOUT = 'SET_TO_CHECKOUT';
const ORDER_SUCCES = 'ORDER_SUCCES';
const UPDATE_PRICE = 'UPDATE_PRICE';
const ERROR = 'ERROR';
const POP_UP_OPEN = 'POP_UP_OPEN';
const POP_UP_CLOSE = 'POP_UP_CLOSE';
const START_PLACE_ORDER = 'START_PLACE_ORDER';
/**--------------------- Action creator----------------- */

/* het popup mandje dat openspringt bij het toevoegen van een product
	Dit gebeurd ook bij het toeveigen van een nieuw item */
export const closePopupBasket = () => (dispatch) => {
	dispatch({ type: POP_UP_CLOSE });
};
/*  Het toevoegen van een item: als een item toe gevoegd wordt, zal 
	gecontroleerd of het een nieuw item is of één waarvan het aantal verhoogd moet worden */
export const addItem = (itemId, newItem = false) => (dispatch) => {
	// zet loading aan, en geeft message
	dispatch(addItemInit());

	// als het nog niet aanwezig is het product ophalen (api)
	if (newItem) {
		dispatch({ type: POP_UP_OPEN });
		// samenstellen zoekopdracht
		const iri = 'products/' + itemId + '.jsonld';
		axios
			.get(iri)
			.then((result) => {
				const data = result.data;
				const product = {
					id: data.id,
					price: data.productPrice['price'],
					taxrate: data.productPrice['taxrate'],
					quantity: 1,
					name: data.title,
					miniDiscription: data.miniDescription,
					promo: data.articlePromotionArray.amount ? data.articlePromotionArray.amount : false,
					promoDescription: data.articlePromotionArray.discr ? data.articlePromotionArray.discr : false,
					comment: '',
					product: data['@id'],
					image: data.images[0].image
				};

				dispatch(addNewItem(product, itemId));
			})
			.catch((e) => {});
		// als het geen nieuw item is wordt het aantal aangepast
	} else {
		dispatch(addOneToItem(itemId));
	}
};

/* om een item te verwijderen, wordt er gecontroleerd
  of het 1 item is of het volledige product */
export const removeItem = (itemId, removeOne = false) => (dispatch) => {
	if (removeOne) {
		dispatch(removeOneFromItem(itemId));
	} else {
		console.log('in remove complete');
		dispatch(removeItemFromIdListAndOrderlist(itemId));
	}
};

/*  De actie die instaat om de prijzen en promoties up to date te houden 
	dit gebeurd bij het laden van de applicatie en bij het aanmaken van een bestelling. */
export const updatePrices = (idList) => (dispatch) => {
	idList.map((id) => {
		axios.get('products/' + id + '.jsonld').then((result) => {
			const promo = result.data.articlePromotionArray.amount ? result.data.articlePromotionArray.amount : false;
			const promoDescription = result.data.articlePromotionArray.discr
				? result.data.articlePromotionArray.discr
				: false;
			const productPrice = result.data.productPrice['price'];
			const image = result.data.images[0].image;

			dispatch({ type: UPDATE_PRICE, payload: { productPrice, id, promo, promoDescription, image } });
		});
		return true;
	});
};

/* Bij he plaatsen van een promo wordt de prijs meegegven die de user te zien kreeg */
export const placeOrder = (products) => (dispatch) => {
	dispatch({ type: START_PLACE_ORDER });
	const orderDetails = products.map((product) => {
		let productPrice = product.price;
		const oldPrice = productPrice;
		if (product.promo) {
			const promo = productPrice / 100 * product.promo;
			productPrice = productPrice - promo;
		}
		return {
			price: parseFloat(productPrice.toFixed(2)),
			taxrate: product.taxrate,
			quantity: product.quantity,
			product: product.product,
			comment: product.promo ? `promo-prijs:  ${product.promo} %  originele prijs ` : `${oldPrice}oldPrice : `
		};
	});
	axiosToken
		.post('orders', { status: 'pre-payment', orderDetails })
		.then((result) => {
			dispatch({ type: ORDER_SUCCES });
		})
		.catch((e) => {
			dispatch({
				type: ERROR,
				payload: 'Sorry, er liep iets is met uw bestelling, probeer het later opnieuw aub'
			});
			console.log(e.response);
		});
};

/* acties die nodig zijn om een user recht te geven om naar de login pagina te kunnen  */
export const toCheckout = () => (dispatch) => {
	dispatch({ type: SET_TO_CHECKOUT });
};
// subactions

const addItemInit = () => ({
	type: ADD_ITEM_INIT
});

const addNewItem = (product, itemId) => ({
	type: ADD_NEW_ITEM,
	payload: { product, itemId }
});

const addOneToItem = (itemId) => ({
	type: ADD_QUANTITY_ITEM,
	payload: itemId
});

const removeItemFromIdListAndOrderlist = (itemId) => ({
	type: REMOVE_ITEM,
	payload: itemId
});

const removeOneFromItem = (itemId) => ({
	type: MINUS_QUANTITY_ITEM,
	payload: itemId
});

/**--------------------- Reducer------------------------ */
export default (state = init, { type, payload }) => {
	switch (type) {
		case ADD_ITEM_INIT:
			return {
				...state,
				status: 'in-shop',
				message: 'Artikel wordt toegevoegd',
				loading: true
			};
		case ADD_NEW_ITEM:
			return {
				...state,
				message: 'Artikel toegevoegd',
				loading: false,
				status: 'in-shop',
				orderDetails: [ ...state.orderDetails, payload.product ],
				idList: [ ...state.idList, payload.itemId ]
			};
		case ADD_QUANTITY_ITEM:
			return {
				...state,
				message: false,

				orderDetails: state.orderDetails.map((item) => {
					if (item.id === payload) {
						item.quantity += 1;
						return item;
					} else {
						return item;
					}
				})
			};
		case REMOVE_ITEM:
			return {
				...state,
				message: false,
				orderDetails: state.orderDetails.filter((item) => {
					return item.id !== payload;
				}),
				idList: state.idList.filter((number) => {
					return number !== payload;
				})
			};
		case MINUS_QUANTITY_ITEM:
			return {
				...state,
				orderDetails: state.orderDetails.map((item) => {
					if (item.id === payload) {
						item.quantity -= 1;
						return item;
					} else {
						return item;
					}
				})
			};
		case SET_TO_CHECKOUT:
			return {
				...state,
				toCheckout: true,
				loading: false
			};
		case ORDER_SUCCES:
			return {
				user: false,
				error: false,
				errorMessage: false,
				message: 'uw bestelling is geslaagd',
				loading: false,
				toCheckout: false,
				status: 'pay-ok',
				idList: [],
				orderDetails: []
			};
		case UPDATE_PRICE:
			return {
				...state,
				loading: false,
				error: false,
				orderDetails: state.orderDetails.map((item) => {
					if (item.id === payload.id) {
						item.price = payload.productPrice;
						item.promo = payload.promo;
						item.promoDescription = payload.promoDescription;
						item.image = payload.image;
						return item;
					} else {
						return item;
					}
				})
			};
		case ERROR:
			return {
				...state,
				message: payload,
				error: true
			};
		case POP_UP_OPEN:
			return {
				...state,
				popUp: true
			};
		case POP_UP_CLOSE:
			return {
				...state,
				popUp: false
			};
		case START_PLACE_ORDER:
			return {
				...state,
				loading: true,
				message: 'even geduld, wij verwerken uw bestelling'
			};
		default:
			return state;
	}
};
