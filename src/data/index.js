import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import basketReducer from './basket';
import searchReducer from './search';
import userReducer from './user';
import storage from 'redux-persist/lib/storage';
import { persistCombineReducers, persistStore } from 'redux-persist';

const persistConfig = {
	key: 'root',
	whiteList: [ 'basket', 'user' ],
	storage
};

const rootReducer = persistCombineReducers(persistConfig, {
	search: searchReducer,
	basket: basketReducer,
	user: userReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);
