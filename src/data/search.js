import axios from '../apiSettings/axios';

/**--------------------- initial state------------------ */

const init = {
	search: null,
	error: false,
	message: '',
	loading: false,
	products: [],
	productDetail: false,
	categories: false,
	MainCategories: false,
	catDescripton: false,
	promo: false
};
/**--------------------- Types-------------------------- */
const SEARCH_START = 'SEARCH_START';
const SEARCH_SUCCES = 'SEARCH_SUCCES';
const SEARCH_ERROR = 'SEARCH_ERROR';
const SET_SEARCH_STRING = 'SET_SEARCH_STRING';
const SEARCH_DETAIL_SUCCES = 'SEARCH_DETAIL_SUCCES';
const SEARCH_CATEGORIES_SUCCES = 'SEARCH_CATEGORIES_SUCCES';
const CAT_DESCRIPTION = 'CAT_DESCRIPTION';
const SET_MESSAGE = 'SET_MESSAGE';
const RESET_MESSAGE = 'RESET_MESSAGE';

/**--------------------- Action creator----------------- */

/** Main actions */

/* zoeken naar producten in de overzichtspagina
 eventueel aan de hand van een categorie of een zoekopdracht die in het component samengesteld geweest is */

export const searchProducts = (category = false, searchString = false, searchWord = '') => (dispatch) => {
	dispatch(searchStart('wij zoeken snel even de juiste producten voor u'));
	dispatch(setSearchString(searchString));
	/* aan de hand van de zoekopdracht de url samen stellen  */
	let searchUri = '';
	if (searchString || category) {
		searchUri = '?';
		if (searchString) {
			searchUri += searchString;
			searchUri += category ? '&' : '';
		}
		if (category) {
			searchUri += 'categories.id=' + category;
		}
	}
	axios
		.get(`products.jsonld${searchUri}`)
		.then((result) => {
			/* Controlleren of er producten gevonden zijn , dan om een extra beveiliging in te bouwen,
			controlleren dat er zeker geen producten zijn meegekomen zonder prijs en deze eruit filteren */
			if (result.data) {
				const products = result.data['hydra:member'].filter((item) => {
					return item['productPrice']['price'] !== undefined;
				});
				if (products.length === 0) {
					dispatch(searchError(`Sorry geen producten gevonden onder de zoekterm ${searchWord}.`));
				} else {
					dispatch(searchSucces(products, searchWord));
				}
			} else {
				dispatch(searchError('Sorry geen producten gevonden.'));
			}
		})
		.catch((e) => {
			dispatch(searchError('Sorry geen producten gevonden.'));
		});
};

// zoeken naar 1 product aan de hand van het porduct id
export const searchDetail = (id) => (dispatch) => {
	dispatch(searchStart());
	const searchUrl = 'products/' + id + '.jsonld';
	axios
		.get(searchUrl)
		.then((result) => {
			// controleren of er een product gevonden is
			if (result.data) {
				//controleren of  het product zeker een prijs heeft

				if (!result.data) {
					dispatch(searchError('Sorry er liep iets mis met het laden van uw product'));
				} else {
					let promo = false;
					if (result.data.articlePromotionArray.amount) {
						promo = true;
					}
					dispatch(SearchDetailSucces(result.data, promo));
				}
			} else {
			}
		})
		.catch((e) => {
			dispatch(searchError('Sorry er liep iets mis met het laden van uw product'));
		});
};
/* 
 Hier kunnen de categorieën gezocht worden, bij het eerste bezoek worden de hoofd categorieën geladen
 zodoenden zij maar één keer geladen moeten worden 
 Deze functie wordt ook gebruikt details van categorieën op te halen en om sub-categorieën te laden
 */
export const getCategories = (catId = false, getMainCategories = false, storeMainCat = false) => (dispatch) => {
	dispatch(searchStart('wij zoeken voor u even onze categorieën'));

	/* Eerst wordt er de url samengesteld aan de hand van de zoekopdracht
		dwz, zijn het hoofdcategorieën, subcategorieën van deze die 
		we nodig hebben.
	*/
	let parent = '';
	if (catId) {
		parent = '?parent.id=' + catId;
	}
	axios
		.get(`categories.jsonld${parent}`)
		.then((result) => {
			// controleren of er resultaten zijn
			let categorie = result.data['hydra:member'];
			if (categorie) {
				// enkel als er child categorieën zijn deze weergeven
				if (catId) {
					// filteren naar subCategorieën
					categorie = result.data['hydra:member'].filter((cat) => {
						return parseInt(cat.id) !== parseInt(catId);
					});
				}
				// De hoofdcategorieën eruit halen
				if (getMainCategories) {
					categorie = result.data['hydra:member'].filter((cat) => {
						return cat.id === cat.parentById;
					});
				}
				// bij succes  de categorieën opslagen
				dispatch(categoriesSucces(categorie, storeMainCat));
			}
		})
		.catch((error) => {
			dispatch(searchError('Sorry er liep iets mis met het laden van uw categorieën'));
		});
};

/* De details opvragen van een specifieke categorie */
export const getCatDetails = (catId) => (dispatch) => {
	axios.get(`categories/${catId}.jsonld`).then((result) => {
		dispatch({ type: CAT_DESCRIPTION, payload: result.data });
	});
};

/**-----action helpers */

export const setMessage = (message) => (dispatch) => {
	dispatch({ type: SET_MESSAGE, payload: message });
};

export const resetMessge = () => (dispatch) => {
	dispatch({ type: RESET_MESSAGE });
};

const searchStart = (message = '') => ({
	type: SEARCH_START,
	payload: message
});
const searchSucces = (product, searchString) => ({
	type: SEARCH_SUCCES,
	payload: { product, searchString }
});

const searchError = (errorMessage) => ({
	type: SEARCH_ERROR,
	payload: errorMessage
});

const setSearchString = (str) => ({
	type: SET_SEARCH_STRING,
	payload: str
});

const SearchDetailSucces = (product, promo) => ({
	type: SEARCH_DETAIL_SUCCES,
	payload: { product, promo }
});

const categoriesSucces = (categories, storeMainCat) => ({
	type: SEARCH_CATEGORIES_SUCCES,
	payload: { categories, storeMainCat }
});

/**--------------------- Reducer------------------------ */
export default (state = init, { type, payload }) => {
	switch (type) {
		case SEARCH_START:
			return {
				...state,
				message: payload,
				loading: true,
				promo: false
			};
		case SEARCH_SUCCES:
			return {
				...state,
				message: `er zijn ${payload.product
					.length} producten gevonden voor u , met zoekoprdracht:   ${payload.searchString}`,
				productDetail: false,
				loading: false,
				products: [ ...payload.product ]
			};
		case SEARCH_ERROR:
			return {
				...state,
				loading: false,
				error: true,
				products: [],
				message: payload
			};
		case SET_SEARCH_STRING:
			return {
				...state,
				search: payload
			};
		case SEARCH_DETAIL_SUCCES:
			return {
				...state,
				search: '',
				loading: false,
				productDetail: { ...payload.product },
				message: 'product gevonden',
				promo: payload.promo
			};
		case SEARCH_CATEGORIES_SUCCES:
			if (payload.storeMainCat) {
				return {
					...state,
					message: '',
					loading: false,
					MainCategories: [ ...payload.categories ]
				};
			} else {
				return {
					...state,
					message: '',
					loading: false,
					categories: [ ...payload.categories ]
				};
			}
		case CAT_DESCRIPTION:
			return {
				...state,
				catDescripton: payload
			};
		case SET_MESSAGE:
			return {
				...state,
				message: payload
			};
		case RESET_MESSAGE:
			return {
				...state,
				message: false
			};

		default:
			return state;
	}
};
