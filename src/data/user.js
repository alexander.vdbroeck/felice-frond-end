import axios from '../apiSettings/axios';
import axiosToken from '../apiSettings/axiosToken';

/**--------------------- initial state------------------ */
const init = {
	userInfo: {
		email: '',
		gender: '',
		firstName: '',
		secondName: '',
		birthday: ''
	},
	deliveryAddress: false,
	billingAddress: false,
	error: false,
	loading: false,
	message: false,
	token: false,
	loggedIn: false,
	cookies: false,
	loggedInCheck: false,
	updateSucces: false
};
/**--------------------- Types-------------------------- */
const SET_USER_DATA = 'SET_USER_DATA';
const INIT_SEARCH = 'INIT_SEARCH';
const ERROR = 'ERROR';
const SET_USER = 'SET_USER';
const SET_DELIV_ADDRESS = 'SET_DELIV_ADDRESS';
const SET_BILLING_ADDRESS = 'SET_BILLING_ADDRESS';
const RESET_USER = 'RESET_USER';
const PROCEED_LOGGED_IN = 'PROCEED_LOGGED_IN';
const ERASE_USER = 'ERASE_USER';
const CLOSE_COOKIE = 'CLOSE_COOKIE';

/**--------------------- Action creator----------------- */

/** Main actions */

/* een user uitloggen, en zijn gegevens wissen uit de local storage */

export const resetUser = () => (dispatch) => {
	dispatch({ type: RESET_USER });
};
/* Gegevens ophalen van de gebruiker, voornamelijk om zijn meest recente
 	adres gegevens te hebben  */
export const getUserInfo = (userId) => (dispatch) => {
	console.log('test');
	dispatch({ type: PROCEED_LOGGED_IN, payload: false });
	axiosToken
		.get(`users/${userId}`)
		.then((result) => {
			const info = result.data;
			// de gegevens van de gebruiker ophalen
			console.log(result);
			const userInfo = {
				email: info.email ? info.email : '',
				gender: info.gender ? info.gender : '',
				firstName: info.firstName ? info.firstName : '',
				secondName: info.secondName ? info.secondName : ''
			};

			// controlleren of de gebruiker reeds adressen heeft.

			dispatch(setUserData({ userInfo }));
			// Leverings addres??
			console.log(info.deliveryAddress);

			if (info.deliveryAddress) {
				dispatch(setDelivAddress(info.deliveryAddress));
			}

			// facturatie addres?
			if (info.billingAddress) {
				dispatch(setBillingAddress(info.billingAddress));
			}
			dispatch(setUserData({ userInfo }));
			//To gegevens in state zetten.
		})
		.catch((e) => {
			// to do errors bij het falen van de user gegevens op te halen.
			console.log(e);
			dispatch({ type: ERASE_USER });
		});
};
//registreren van een user
export const registerUser = (email, pass, firstName, secondName, gender) => (dispatch) => {
	const iri = 'users.jsonld';
	dispatch(initSearch());
	axios
		.post(iri, {
			email,
			password: pass,
			firstName,
			secondName,
			gender
		})
		.then((result) => {
			// als er geen errors zijn de user inloggen anders errors tonen
			axios
				.post('userlogin', {
					email: email,
					password: pass
				})
				.then((result) => {
					const id = result.data.data.id;
					const token = result.data.token;
					localStorage.setItem('token', token);
					localStorage.setItem('id', id);
					dispatch(setUser());
				})
				.catch((e) => {
					// errors van het inloggen.
					let error = e.response.data['hydra:description'];
					if (error) {
						if (error === 'email: This value is already used.') {
							error = 'dit e-mail adres is reeds in gebruik';
						}
						dispatch(setError(error));
					} else {
						dispatch(setError('oeps ...er liep iets mis'));
					}
				});
		})
		.catch((e) => {
			// errors van het registreren en vertalen
			let error = e.response.data['hydra:description'];
			if (error) {
				if (error === 'email: This value is already used.') {
					error = 'dit e-mail adres is reeds in gebruik';
				}
				dispatch(setError(error));
			} else {
				dispatch(setError('oeps ... someting went wrong'));
			}
		});
};

/* Inloggen van een user  */
export const logIn = (email, pass) => (dispatch) => {
	dispatch(initSearch());
	axios
		.post('userlogin', {
			email: email,
			password: pass
		})
		.then((result) => {
			const id = result.data.data.id;
			const token = result.data.token;
			dispatch(setUser());
			// gegevens in local storage opslaan
			localStorage.setItem('token', token);
			localStorage.setItem('id', id);
		})
		.catch((e) => {
			// gepaste errors maken

			console.log(e.response.data.message);
			if (!e.response.data.message) {
				dispatch(setError('Oeps er liep iets mis, probeer het later nog eens op nieuw'));
			} else {
				if (e.response.data.message) {
					console.log(e.response.data.message);
					let error = e.response.data.message;
					if (error) {
						if (error === 'Invalid credentials.') {
							error = 'Uw inloggegevens zijn niet correct';
						}
						dispatch(setError(error));
					}
				}
			}
		});
};

/*  het aanmaken van een nieuw addres met een post request */
export const newAddress = (street, housenr, city, busnr = '', addressType = 'deliv') => (dispatch) => {
	// to do initieren zoekactie
	dispatch(initSearch());
	axiosToken
		.post('addresses', {
			addressType,
			street,
			housenr,
			city: `/api/cities/${city}`,
			busnr
		})
		.then((result) => {
			if (addressType === 'deliv') {
				dispatch(setDelivAddress(result.data));
			} else {
				dispatch(setBillingAddress(result.data));
			}
		})
		.catch((e) => {
			dispatch(setError('er liep iets mis me het opslaan van uw adres'));
		});
};

/* addressen wijzigen met een Put request */
export const edditAddres = (addressId, street, housenr, busnr, city, addressType = 'deliv') => (dispatch) => {
	// to do initieren zoekactie
	dispatch(initSearch());
	axiosToken
		.put('addresses/' + addressId, {
			addressType,
			street,
			housenr,
			busnr,
			city: `/api/cities/${city}`
		})
		.then((result) => {
			console.log(result);
			if (addressType === 'deliv') {
				dispatch(setDelivAddress(result.data));
			} else {
				dispatch(setBillingAddress(result.data));
			}
		})
		.catch((e) => {
			dispatch(setError('er liep iets mis me het opslaan van uw adres'));
		});
};

export const cookieConfirm = () => (dispatch) => {
	dispatch({ type: CLOSE_COOKIE });
};

export const proceedLoggedIn = (value) => (dispatch) => {
	dispatch({ type: PROCEED_LOGGED_IN, payload: value });
};

const initSearch = () => ({
	type: INIT_SEARCH
});
const setError = (message) => ({
	type: ERROR,
	payload: message
});

const setUser = () => ({
	type: SET_USER
});

const setUserData = (user) => ({
	type: SET_USER_DATA,
	payload: user
});

const setDelivAddress = (address) => ({
	type: SET_DELIV_ADDRESS,
	payload: {
		addresId: address.id,
		street: address.street,
		houseNr: address.housenr,
		busNr: address.busnr ? address.busnr : '',
		cityId: address.city.id,
		city: address.city.name,
		postCode: address.city.zipcode
	}
});
const setBillingAddress = (address) => ({
	type: SET_BILLING_ADDRESS,
	payload: {
		addresId: address.id,
		street: address.street,
		houseNr: address.housenr,
		busNr: address.busnr ? address.busnr : '',
		cityId: address.city.id,
		city: address.city.name,
		postCode: address.city.zipcode
	}
});

/**--------------------- Reducer------------------------ */
export default (state = init, { type, payload }) => {
	switch (type) {
		case INIT_SEARCH:
			return {
				...state,
				loading: true,
				message: 'wij halen uw gegevens op',
				error: false,
				addresComfirm: false,
				updateSucces: false
			};
		case ERROR:
			return {
				...state,
				error: true,
				message: payload,
				loading: false,
				email: '',
				token: '',
				loggedIn: true
			};
		case SET_USER:
			return {
				...state,
				message: false,
				error: false,
				loading: false,
				loggedIn: true,
				toBasket: true,
				loggedInCheck: true,
				deliveryAddress: false,
				billingAddress: false,
				updateSucces: false
			};
		case SET_USER_DATA:
			return {
				...state,
				loading: false,
				message: false,
				error: false,
				userInfo: payload.userInfo
			};
		case SET_DELIV_ADDRESS:
			return {
				...state,
				loading: false,
				message: 'uw leverings adres is opgeslagen',
				deliveryAddress: payload,
				updateSucces: true
			};
		case SET_BILLING_ADDRESS:
			return {
				...state,
				loading: false,
				message: 'uw facturatie adres is opgeslagen',
				billingAddress: payload,
				updateSucces: true
			};
		case RESET_USER:
			return {
				...state,
				error: false,
				loading: false,
				message: 'checkout-completed',
				addresComfirm: false
			};
		case PROCEED_LOGGED_IN:
			return {
				...state,
				loggedInCheck: payload
			};
		case ERASE_USER:
			return {
				...state,
				userInfo: {
					email: '',
					gender: '',
					firstName: '',
					secondName: '',
					birthday: ''
				},
				loggedIn: false,
				deliveryAddress: false,
				billingAddress: false
			};
		case CLOSE_COOKIE:
			return {
				...state,
				cookies: true
			};
		default:
			return state;
	}
};
