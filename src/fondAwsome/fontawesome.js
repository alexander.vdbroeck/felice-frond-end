import { library } from '@fortawesome/fontawesome-svg-core';
import { faHeart } from '@fortawesome/free-regular-svg-icons';
import {
	faMobileAlt,
	faBars,
	faTimesCircle,
	faShoppingBag,
	faSearch,
	faEllipsisV,
	faArrowLeft,
	faPlus,
	faMinus
} from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';

library.add(
	faFacebook,
	faInstagram,
	faShoppingBag,
	faSearch,
	faTwitter,
	faYoutube,
	faHeart,
	faMobileAlt,
	faBars,
	faTimesCircle,
	faEllipsisV,
	faArrowLeft,
	faPlus,
	faMinus
);
