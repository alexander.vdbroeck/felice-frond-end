export const getBackground = () => (catName) => {
	let img = process.env.REACT_APP_BASE_URI_BACKGROUND;
	switch (catName) {
		case 'Cards':
			img += 'Cards.jpg';
			break;
		case 'cards':
			img += 'Cards.jpg';
			break;
		case 'rouw-kaart':
			img += 'Cards.jpg';
			break;
		case 'love-you-kaarten':
			img += 'Cards.jpg';
			break;
		case 'sweets':
			img += 'sweets.jpg';
			break;
		case 'schenk-snoep':
			img += 'sweets.jpg';
			break;
		case 'schep-snoep':
			img += 'sweets.jpg';
			break;
		case 'gifts':
			img += 'gifts.jpg';
			break;
		case 'valentijn':
			img += 'gifts.jpg';
			break;
		case 'gifts':
			img += 'mugs.jpg';
			break;
		case 'only-by-Felice':
			img += 'sweets.jpg';
			break;
		case 'cottoncandy':
			img += 'cottoncandy.jpg';
			break;
		case 'candy-cake':
			img += 'sweets.jpg';
			break;
		default:
			img += 'Cards.jpg';
	}
	return img;
};
