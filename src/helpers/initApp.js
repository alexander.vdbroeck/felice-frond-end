import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCategories } from '../data/search';
import { updatePrices } from '../data/basket';

const dispatch = useDispatch();
export const init = () => (dispatch) => {
	const { orderDetails, idList } = useSelector((state) => state.basket);
	const empty = orderDetails.length == 0;
	// als er bij het laden van de applicatie reeds producten in het mandje zitten
	// wordt er op de prijs een update uitgevoerd.
	if (!empty) {
		dispatch(updatePrices(idList));
	}
	dispatch(getCategories(false, true, true));
};
