import { useState } from 'react';

export const useField = (state = '') => {
	const [ value, setvalue ] = useState(state);
	const onChange = (e) => {
		setvalue(e.target.value);
	};
	return { value, onChange, setvalue };
};
